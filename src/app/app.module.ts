import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './components/users/users.component';
import { LoginComponent } from './components/login/login.component';
import { FormsModule } from '@angular/forms';
import { ContactsComponent } from './components/contacts/contacts.component';
import { AuthGuard } from './guards/auth.guard';

const routes:Routes = [
	{path:'users', component:UsersComponent},
  {path:'login', component:LoginComponent},
  {path:'contacts', component:ContactsComponent, canActivate:[AuthGuard]},
	{path:'', pathMatch:'full', redirectTo:'users'}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ContactsComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, RouterModule.forRoot(routes), FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
