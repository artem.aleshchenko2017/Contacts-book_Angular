import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginRequest } from '../model/login-request';
import { RegisterRequest } from '../model/register-request';
import { Observable, Subject } from 'rxjs';
import { LoginResponse } from '../model/login-response';
import { RegisterResponse } from '../model/register-response';
import { Links } from '../config/Links';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
	private token: string|null = null;
  
	public authSubject:Subject<null> = new Subject<null>();
	public logoutSubject:Subject<null> = new Subject<null>();

  constructor(@Inject(HttpClient) private http:HttpClient) { }

  public auth(LoginRequest:LoginRequest):Observable<LoginResponse>{
  	return this.http.post(Links.url('/login'), LoginRequest)
  		.pipe(map(resp => LoginResponse.fromJson(resp)))
  		.pipe(tap(r => {
  			if(r.isSeccessful()){
  				this.authenticate(r.token);
  			}
  		}))
  }

  public register(r:RegisterRequest):Observable<RegisterResponse>{
  	return this.http.post(Links.url('/register'),r.toJson())
  		.pipe(map(resp => RegisterResponse.fromJson(resp)))
  }

  private authenticate(token: string | null){
  	this.token = token;
  	this.authSubject.next();
  }

  public logout(){
  	this.token = null;
  	this.logoutSubject.next();
  }

  public getToken():string|null{
  	return this.token;
  }

  public isAuth():boolean{
  	return this.token!=null;
  }
}
