import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { LoginRequest } from '../model/login-request';
// import { RegisterRequest } from '../model/register-request';
import { Observable} from 'rxjs';
// import { LoginResponse } from '../model/login-response';
// import { RegisterResponse } from '../model/register-response';
import { Links } from '../config/Links';
import { map } from 'rxjs/operators';
import { GetUsersResponse } from '../model/get-user-response';

@Injectable({providedIn: 'root'})
export class UsersService {

  constructor(@Inject(HttpClient) private http:HttpClient) { }

  public getall():Observable<GetUsersResponse>{
  	return this.http.get(Links.url('/users'))
  		.pipe(map(resp => GetUsersResponse.fromJson(resp)))
  }
}
