import { Component, OnInit, Inject } from '@angular/core';
import { ContactsService } from '../../services/contacts.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {

  constructor(@Inject(ContactsService) private contactsService:ContactsService) { }

  ngOnInit(): void {
  	this.contactsService.getContacts().subscribe(()=>{})
  }

}
