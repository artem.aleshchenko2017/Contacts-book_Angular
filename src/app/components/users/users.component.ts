import { Component,Inject, OnDestroy, OnInit } from '@angular/core';
// import { AuthService } from '../../services/auth.service';
import { User } from '../../model/user';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnDestroy{

	users:User[] = [];
	constructor(@Inject(UsersService) private usersService:UsersService){
	}


	ngOnDestroy():void{
		this.users = [];
	}

	ngOnInit():void{
		this.usersService.getall().subscribe(r =>{
			if(r.users!=null )this.users = r.users
		});
	}
}	
