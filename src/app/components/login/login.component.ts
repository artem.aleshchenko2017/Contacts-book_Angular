import { Component, Inject, OnInit } from '@angular/core';
import { LoginRequest } from '../../model/login-request';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  public LoginRequest = new LoginRequest('','')
  public error:string|null = null;

  constructor(@Inject(AuthService) private authService:AuthService){

  }

  login(){
  	this.authService.auth(this.LoginRequest)
  		.subscribe(r=>{
  			if(!r.isSeccessful()) this.error = r.error;
  		}, ()=>this.error="unexpected fail");
  }

  ngOnInit():void{
  	this.error = null;
  	this.LoginRequest.clear();
  }
}
